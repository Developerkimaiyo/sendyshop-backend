package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/controllers"
)

func Setup(app *fiber.App) {

	//mobile
	app.Post("/api/register", controllers.Register)
	app.Post("/api/login", controllers.Login)
	app.Post("/api/logout", controllers.Logout)
	app.Post("/api/forgot", controllers.Forgot)
	app.Post("/api/reset", controllers.Reset)

	app.Get("/api/latestbrand", controllers.GetLatestBrands)

	app.Get("/api/brand", controllers.AllBrands)
	app.Post("/api/brand", controllers.CreateBrand)
	app.Put("/api/brand/:id", controllers.UpdateBrand)
	app.Delete("/api/brand/:id", controllers.DeleteBrand)

	app.Get("/api/category", controllers.AllCategories)
	app.Post("/api/category", controllers.CreateCategory)
	app.Put("/api/category/:id", controllers.UpdateCategory)
	app.Delete("/api/category/:id", controllers.DeleteCategory)

	app.Get("/api/sellerproducts", controllers.GetSellerProducts)
	app.Get("/api/relatedproducts", controllers.GetRelatedProducts)
	app.Get("/api/latestproducts", controllers.GetLatestProducts)
	app.Get("/api/brandProducts", controllers.GetBrandProducts)
	app.Get("/api/categoryProducts", controllers.GetCategoryProducts)

	app.Get("/api/product", controllers.AllProducts)
	app.Post("/api/product", controllers.CreateProduct)
	app.Put("/api/product/:id", controllers.UpdateProduct)
	app.Delete("/api/product/:id", controllers.DeleteProduct)

	app.Get("/api/order", controllers.AllOrders)
	app.Post("/api/order", controllers.CreateOrder)
	app.Put("/api/order/:id", controllers.UpdateOrder)
	app.Delete("/api/order/:id", controllers.DeleteOrder)

	app.Put("/api/status/:id", controllers.UpdateOrderStatus)

	app.Get("/api/pending", controllers.GetPendingOrders)
	app.Get("/api/processing", controllers.GetProcessingOrders)
	app.Get("/api/transit", controllers.GetTransitOrders)
	app.Get("/api/delivered", controllers.GetDeliveredOrders)
	app.Get("/api/cancel", controllers.GetCancelOrders)

	app.Get("/api/productwishlist", controllers.GetProductWishList)

	// app.Use(middlewares.IsAuthenticated)

	app.Get("/api/user", controllers.User)
	app.Put("/api/update", controllers.UpdateInfo)

	app.Get("/api/address", controllers.AllAddress)
	app.Get("/api/address/:id", controllers.GetAddress)
	app.Post("/api/address", controllers.CreateAddress)
	app.Put("/api/address/:id", controllers.UpdateAddress)
	app.Delete("/api/address/:id", controllers.DeleteAddress)

	app.Get("/api/wishlist", controllers.GetUserWishList)
	app.Post("/api/wishlist", controllers.CreateWishList)
	app.Delete("/api/wishlist/:id", controllers.DeleteWishList)

	app.Get("/api/permissions", controllers.AllPermissions)

	app.Get("/api/roles", controllers.AllRoles)
	app.Post("/api/roles", controllers.CreateRole)
	app.Get("/api/roles/:id", controllers.GetRole)
	app.Put("/api/roles/:id", controllers.UpdateRole)
	app.Delete("/api/roles/:id", controllers.DeleteRole)

	app.Get("/api/users", controllers.AllUsers)
	app.Post("/api/users", controllers.CreateUser)
	app.Get("/api/users/:id", controllers.GetUser)
	app.Put("/api/users/:id", controllers.UpdateUser)
	app.Delete("/api/users/:id", controllers.DeleteUser)

	app.Post("/api/upload", controllers.Upload)
	app.Static("/api/uploads", "./uploads")

}
