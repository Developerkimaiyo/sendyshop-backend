module github.com/sendy/sendyshop

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.10.0
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e
	gorm.io/driver/mysql v1.1.0
	gorm.io/gorm v1.21.10
)
