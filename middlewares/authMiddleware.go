package middlewares

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/util"
)

func IsAuthenticated(c *fiber.Ctx) error {
	bearToken := c.Get("Authorization")
	onlyToken := strings.Split(bearToken, " ")
	if _, err := util.ParseJwt(onlyToken[1]); err != nil {
		c.Status(fiber.StatusUnauthorized)
		return c.JSON(fiber.Map{
			"message": "unauthenticated",
		})
	}

	return c.Next()
}
