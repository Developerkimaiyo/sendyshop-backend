package database

import (
	"github.com/sendy/sendyshop/models"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func Connect() {
	database, err := gorm.Open(mysql.Open("root:password@/sendyshop"), &gorm.Config{})

	if err != nil {
		panic("Could not connect to the database")
	}

	DB = database

	database.AutoMigrate(&models.User{}, &models.PasswordReset{}, &models.Role{}, &models.Permission{}, &models.Address{}, &models.Brand{}, &models.Product{}, &models.Category{}, &models.Order{}, &models.OrderItem{}, &models.WishList{})

}
