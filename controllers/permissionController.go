package controllers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/database"
	"github.com/sendy/sendyshop/models"
)

func AllPermissions(c *fiber.Ctx) error {
	var permissions []models.Permission

	database.DB.Find(&permissions)

	return c.JSON(permissions)
}

func CreatePermissions(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	permission := models.Permission{
		Name: data["name"],
	}
	database.DB.Create(&permission)

	return c.JSON(permission)

}
