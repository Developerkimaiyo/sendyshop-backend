package controllers

import (
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/database"
	"github.com/sendy/sendyshop/models"
	"github.com/sendy/sendyshop/util"
)

func GetUserWishList(c *fiber.Ctx) error {
	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	id, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(id)

	var wishList []models.WishList

	database.DB.Where("user_id = ?", userId).Find(&wishList)

	return c.JSON(wishList)
}

func GetProductWishList(c *fiber.Ctx) error {

	productid, _ := strconv.Atoi(c.Query("productId"))

	var wishList []models.WishList

	database.DB.Where("product_id = ?", productid).First(&wishList)

	return c.JSON(wishList)

}

func CreateWishList(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	id, _ := util.ParseJwt(onlyToken[1])
	productId := data["product_id"]
	categoryId := data["category_id"]
	brandId := data["brand_id"]

	userId, _ := strconv.Atoi(id)
	productid, _ := strconv.Atoi(productId)
	categoryid, _ := strconv.Atoi(categoryId)
	brandid, _ := strconv.Atoi(brandId)

	wishList := models.WishList{
		UserID:        uint(userId),
		ProductID:     uint(productid),
		CategoryID:    uint(categoryid),
		BrandID:       uint(brandid),
		Name:          data["name"],
		Thumbnail:     data["thumbnail"],
		Details:       data["details"],
		UnitPrice:     data["unit_price"],
		PurchasePrice: data["purchase_price"],
		Tax:           data["tax"],
		TaxType:       data["tax_type"],
		Discount:      data["discount"],
		DiscountType:  data["Discount_type"],
		CurrentStock:  data["current_stock"],
		Status:        data["status"],
	}
	database.DB.Create(&wishList)

	return c.JSON(fiber.Map{
		"message": "Added successfully",
	})
}

func DeleteWishList(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	WishList := models.WishList{
		Id: uint(id),
	}

	database.DB.Delete(&WishList)

	return c.JSON(fiber.Map{
		"message": "Removed successfully",
	})
}
