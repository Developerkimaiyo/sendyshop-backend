package controllers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/database"
	"github.com/sendy/sendyshop/models"
)

func AllBrands(c *fiber.Ctx) error {
	var brands []models.Brand

	database.DB.Find(&brands)

	return c.JSON(brands)
}

func GetLatestBrands(c *fiber.Ctx) error {
	var brands []models.Brand

	database.DB.Order("id desc").Limit(8).Find(&brands)

	return c.JSON(brands)
}

func CreateBrand(c *fiber.Ctx) error {
	var brand models.Brand

	if err := c.BodyParser(&brand); err != nil {
		return err
	}

	database.DB.Create(&brand)

	return c.JSON(brand)
}

func GetBrand(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	brand := models.Brand{
		Id: uint(id),
	}

	database.DB.Find(&brand)

	return c.JSON(brand)
}

func UpdateBrand(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	brand := models.Brand{
		Id: uint(id),
	}

	if err := c.BodyParser(&brand); err != nil {
		return err
	}

	database.DB.Model(&brand).Updates(brand)

	return c.JSON(brand)
}

func DeleteBrand(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	brand := models.Brand{
		Id: uint(id),
	}

	database.DB.Delete(&brand)

	return nil
}
