package controllers

import (
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/database"
	"github.com/sendy/sendyshop/models"
	"github.com/sendy/sendyshop/util"
)

func AllAddress(c *fiber.Ctx) error {
	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	id, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(id)

	var address []models.Address

	database.DB.Where("user_id = ?", userId).Find(&address)

	return c.JSON(address)
}
func GetAddress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	address := models.Address{
		Id: uint(id),
	}

	database.DB.Find(&address)

	return c.JSON(address)
}

func CreateAddress(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	id, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(id)

	address := models.Address{
		UserID:      uint(userId),
		ContactName: data["contact_name"],
		AddressType: data["address_type"],
		Address:     data["address"],
		City:        data["city"],
		Zip:         data["zip"],
		Phone:       data["phone"],
	}
	database.DB.Create(&address)
	return c.JSON(fiber.Map{
		"message": "Created successfully",
	})
}

func UpdateAddress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	iD, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(iD)

	address := models.Address{
		Id:     uint(id),
		UserID: uint(userId),
	}

	if err := c.BodyParser(&address); err != nil {
		return err
	}

	database.DB.Model(&address).Updates(address)

	return c.JSON(fiber.Map{
		"message": "Updated successfully",
	})
}

func DeleteAddress(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	address := models.Address{
		Id: uint(id),
	}

	database.DB.Delete(&address)

	return c.JSON(fiber.Map{
		"message": "Address Deleted successfully",
	})
}
