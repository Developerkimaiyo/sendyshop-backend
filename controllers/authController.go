package controllers

import (
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/database"
	"github.com/sendy/sendyshop/models"
	"github.com/sendy/sendyshop/util"
)

func Register(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	user := models.User{
		FirstName: data["f_name"],
		LastName:  data["l_name"],
		Email:     data["email"],
		Phone:     data["phone"],
		RoleId:    3,
	}

	user.SetPassword(data["password"])

	database.DB.Where("email = ?", data["email"]).First(&user)

	if user.Id != 0 {
		c.Status(404)
		return c.JSON(fiber.Map{
			"message": "This email already exists",
		})
	} else {

		database.DB.Create(&user)

		token, err := util.GenerateJwt(strconv.Itoa(int(user.Id)))

		if err != nil {
			return c.SendStatus(fiber.StatusInternalServerError)
		}

		cookie := fiber.Cookie{
			Name:     "jwt",
			Value:    token,
			Expires:  time.Now().Add(time.Hour * 24),
			HTTPOnly: true,
		}

		c.Cookie(&cookie)

		return c.JSON(fiber.Map{
			"message": "Registered successfully",
			"token":   token,
			"user":    user,
		})
	}
}

func Login(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	var user models.User

	database.DB.Where("email = ?", data["email"]).First(&user)

	if user.Id == 0 {
		c.Status(404)
		return c.JSON(fiber.Map{
			"message": "Use doesn't exist",
		})
	}

	if err := user.ComparePassword(data["password"]); err != nil {
		c.Status(400)
		return c.JSON(fiber.Map{
			"message": "incorrect password",
		})
	}

	token, err := util.GenerateJwt(strconv.Itoa(int(user.Id)))

	if err != nil {
		return c.SendStatus(fiber.StatusInternalServerError)
	}

	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    token,
		Expires:  time.Now().Add(time.Hour * 24),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)

	return c.JSON(fiber.Map{
		"message": "login successfully",
		"token":   token,
		"user":    user,
	})
}

func User(c *fiber.Ctx) error {
	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	id, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(id)

	var user models.User

	database.DB.Where("id = ?", userId).Preload("Addresses").First(&user)

	return c.JSON(user)

}

func Logout(c *fiber.Ctx) error {
	cookie := fiber.Cookie{
		Name:     "jwt",
		Value:    "",
		Expires:  time.Now().Add(-time.Hour),
		HTTPOnly: true,
	}

	c.Cookie(&cookie)

	return c.JSON(fiber.Map{
		"message": "success",
	})
}

func UpdateInfo(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	id, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(id)

	user := models.User{
		Id:        uint(userId),
		FirstName: data["f_name"],
		LastName:  data["l_name"],
		Email:     data["email"],
		Phone:     data["phone"],
		Shop:      data["shop"],
		RoleId:    3,
	}
	user.SetPassword(data["password"])
	database.DB.Model(&user).Updates(user)

	return c.JSON(fiber.Map{
		"message": "Updated successfully",
	})
}
