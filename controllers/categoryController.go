package controllers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/database"
	"github.com/sendy/sendyshop/models"
)

func AllCategories(c *fiber.Ctx) error {
	var categories []models.Category

	database.DB.Preload("Products").Find(&categories)

	return c.JSON(categories)
}

func CreateCategory(c *fiber.Ctx) error {
	var category models.Category

	if err := c.BodyParser(&category); err != nil {
		return err
	}

	database.DB.Create(&category)

	return c.JSON(category)
}

func GetCategory(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	category := models.Category{
		Id: uint(id),
	}

	database.DB.Find(&category)

	return c.JSON(category)
}

func UpdateCategory(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	category := models.Category{
		Id: uint(id),
	}

	if err := c.BodyParser(&category); err != nil {
		return err
	}

	database.DB.Model(&category).Updates(category)

	return c.JSON(category)
}

func DeleteCategory(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	category := models.Category{
		Id: uint(id),
	}

	database.DB.Delete(&category)

	return nil
}
