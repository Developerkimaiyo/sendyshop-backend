package controllers

import (
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/database"
	"github.com/sendy/sendyshop/models"
	"github.com/sendy/sendyshop/util"
)

func AllOrders(c *fiber.Ctx) error {
	var orders []models.Order

	database.DB.Set("gorm:auto_preload", true).Find(&orders)

	return c.JSON(orders)
}
func GetPendingOrders(c *fiber.Ctx) error {
	var orders []models.Order

	pending := c.Query("pending")
	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	iD, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(iD)

	database.DB.Where("order_status = ? AND user_id = ?", pending, userId).Find(&orders)

	return c.JSON(orders)
}
func GetProcessingOrders(c *fiber.Ctx) error {
	var orders []models.Order

	processing := c.Query("processing")
	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	iD, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(iD)

	database.DB.Where("order_status  = ? AND user_id = ?", processing, userId).Find(&orders)

	return c.JSON(orders)
}
func GetTransitOrders(c *fiber.Ctx) error {
	var orders []models.Order

	transit := c.Query("transit")

	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	iD, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(iD)

	database.DB.Where("order_status  = ? AND user_id = ?", transit, userId).Find(&orders)

	return c.JSON(orders)
}
func GetDeliveredOrders(c *fiber.Ctx) error {
	var orders []models.Order

	delivered := c.Query("delivered")

	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	iD, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(iD)

	database.DB.Where("order_status  = ? AND user_id = ?", delivered, userId).Find(&orders)

	return c.JSON(orders)
}
func GetCancelOrders(c *fiber.Ctx) error {
	var orders []models.Order

	cancel := c.Query("cancel")

	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	iD, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(iD)

	database.DB.Where("order_status  = ? AND user_id = ?", cancel, userId).Find(&orders)

	return c.JSON(orders)
}

func UpdateOrderStatus(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	bearToken := c.Get("Authorization")

	onlyToken := strings.Split(bearToken, " ")

	iD, _ := util.ParseJwt(onlyToken[1])

	userId, _ := strconv.Atoi(iD)

	orders := models.Order{
		Id:     uint(id),
		UserId: uint(userId),
	}

	if err := c.BodyParser(&orders); err != nil {
		return err
	}

	database.DB.Model(&orders).Updates(orders)

	return c.JSON(fiber.Map{
		"message": "Updated successfully",
	})
}

func CreateOrder(c *fiber.Ctx) error {
	var orderDto fiber.Map

	if err := c.BodyParser(&orderDto); err != nil {
		return err
	}

	list := orderDto["order_items"].([]interface{})
	orderItems := make([]models.OrderItem, len(list))

	for i, item := range list {

		productId := item.(map[string]interface{})["product_id"].(string)

		price := item.(map[string]interface{})["price"].(string)
		productName := item.(map[string]interface{})["product_name"].(string)
		image := item.(map[string]interface{})["image"].(string)
		discount := item.(map[string]interface{})["discount"].(string)
		quantity := item.(map[string]interface{})["quantity"].(string)
		taxAmount := item.(map[string]interface{})["tax_amount"].(string)

		newPrice, _ := strconv.ParseFloat(price, 64)
		newQuantity, _ := strconv.ParseFloat(quantity, 64)

		id, _ := strconv.Atoi(productId)

		orderItems[i] = models.OrderItem{
			ProductId:   uint(id),
			Price:       newPrice,
			ProductName: productName,
			Image:       image,
			Discount:    discount,
			Quantity:    newQuantity,
			TaxAmount:   taxAmount,
		}
	}

	cookie := c.Cookies("jwt")

	id, _ := util.ParseJwt(cookie)

	userId, _ := strconv.Atoi(id)

	order := models.Order{
		UserId:            uint(userId),
		DeliveryAddressId: orderDto["delivery_address_id"].(string),
		OrderAmount:       orderDto["order_amount"].(string),
		OrderStatus:       orderDto["order_status"].(string),
		PaymentStatus:     orderDto["payment_status"].(string),
		TotalTaxAmount:    orderDto["total_tax_amount"].(string),
		OrderItems:        orderItems,
	}

	database.DB.Create(&order)

	return c.JSON(order)
}
func UpdateOrder(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	var orderDto fiber.Map

	if err := c.BodyParser(&orderDto); err != nil {
		return err
	}

	list := orderDto["order_items"].([]interface{})
	orderItems := make([]models.OrderItem, len(list))

	for i, item := range list {

		productId := item.(map[string]interface{})["product_id"].(string)

		price := item.(map[string]interface{})["price"].(string)
		productName := item.(map[string]interface{})["product_name"].(string)
		image := item.(map[string]interface{})["image"].(string)
		discount := item.(map[string]interface{})["discount"].(string)
		quantity := item.(map[string]interface{})["quantity"].(string)
		taxAmount := item.(map[string]interface{})["tax_amount"].(string)

		newPrice, _ := strconv.ParseFloat(price, 64)
		newQuantity, _ := strconv.ParseFloat(quantity, 64)

		id, _ := strconv.Atoi(productId)

		orderItems[i] = models.OrderItem{
			ProductId:   uint(id),
			Price:       newPrice,
			ProductName: productName,
			Image:       image,
			Discount:    discount,
			Quantity:    newQuantity,
			TaxAmount:   taxAmount,
		}
	}

	cookie := c.Cookies("jwt")

	userid, _ := util.ParseJwt(cookie)

	userId, _ := strconv.Atoi(userid)

	order := models.Order{
		Id:                uint(id),
		UserId:            uint(userId),
		DeliveryAddressId: orderDto["delivery_address_id"].(string),
		OrderAmount:       orderDto["order_amount"].(string),
		OrderStatus:       orderDto["order_status"].(string),
		PaymentStatus:     orderDto["payment_status"].(string),
		TotalTaxAmount:    orderDto["total_tax_amount"].(string),
		OrderItems:        orderItems,
	}

	database.DB.Updates(&order)

	return c.JSON(order)
}

func DeleteOrder(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	order := models.Order{
		Id: uint(id),
	}
	database.DB.Delete(&order)

	return c.JSON(fiber.Map{
		"message": "Order Deleted successfully",
	})

}
func ClearOrder(c *fiber.Ctx) error {
	cookie := c.Cookies("jwt")

	id, _ := util.ParseJwt(cookie)

	userId, _ := strconv.Atoi(id)

	order := models.Order{
		UserId: uint(userId),
	}

	database.DB.Where("user_id = ?", userId).Unscoped().Delete(&order)

	return c.JSON(order)
}
