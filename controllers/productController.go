package controllers

import (
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/sendy/sendyshop/database"
	"github.com/sendy/sendyshop/models"
	"github.com/sendy/sendyshop/util"
)

func AllProducts(c *fiber.Ctx) error {

	var products []models.Product

	database.DB.Find(&products)

	return c.JSON(products)
}

func GetSellerProducts(c *fiber.Ctx) error {
	var products []models.Product

	id, _ := strconv.Atoi(c.Query("userId"))

	database.DB.Where("user_id = ?", id).Find(&products)

	return c.JSON(products)
}
func GetRelatedProducts(c *fiber.Ctx) error {
	var products []models.Product

	categoryId, _ := strconv.Atoi(c.Query("categoryId"))

	database.DB.Where("category_id = ?", categoryId).Limit(4).Find(&products)

	return c.JSON(products)
}

func GetLatestProducts(c *fiber.Ctx) error {
	var products []models.Product

	database.DB.Order("id desc").Limit(14).Find(&products)

	return c.JSON(products)
}

func GetBrandProducts(c *fiber.Ctx) error {
	var products []models.Product

	brandId, _ := strconv.Atoi(c.Query("brandId"))

	database.DB.Where("brand_id = ?", brandId).Find(&products)

	return c.JSON(products)
}

func GetCategoryProducts(c *fiber.Ctx) error {
	var products []models.Product

	categoryId, _ := strconv.Atoi(c.Query("categoryId"))

	database.DB.Where("category_id = ?", categoryId).Find(&products)

	return c.JSON(products)
}

func CreateProduct(c *fiber.Ctx) error {
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	cookie := c.Cookies("jwt")

	id, _ := util.ParseJwt(cookie)
	categoryId := data["category_id"]
	brandId := data["brand_id"]

	userId, _ := strconv.Atoi(id)
	categoryid, _ := strconv.Atoi(categoryId)
	brandid, _ := strconv.Atoi(brandId)

	product := models.Product{
		UserID:        uint(userId),
		CategoryID:    uint(categoryid),
		BrandID:       uint(brandid),
		Name:          data["name"],
		Thumbnail:     data["thumbnail"],
		Details:       data["details"],
		UnitPrice:     data["unit_price"],
		PurchasePrice: data["purchase_price"],
		Tax:           data["tax"],
		TaxType:       data["tax_type"],
		Discount:      data["discount"],
		DiscountType:  data["Discount_type"],
		CurrentStock:  data["current_stock"],
		Status:        data["status"],
	}
	database.DB.Create(&product)

	return c.JSON(product)
}

func GetProduct(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	product := models.Product{
		Id: uint(id),
	}

	database.DB.Find(&product)

	return c.JSON(product)
}

func UpdateProduct(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))
	var data map[string]string

	if err := c.BodyParser(&data); err != nil {
		return err
	}

	cookie := c.Cookies("jwt")

	userID, _ := util.ParseJwt(cookie)
	categoryId := data["category_id"]
	brandId := data["brand_id"]

	userId, _ := strconv.Atoi(userID)
	categoryid, _ := strconv.Atoi(categoryId)
	brandid, _ := strconv.Atoi(brandId)
	product := models.Product{
		Id:            uint(id),
		UserID:        uint(userId),
		CategoryID:    uint(categoryid),
		BrandID:       uint(brandid),
		Name:          data["name"],
		Thumbnail:     data["thumbnail"],
		Details:       data["details"],
		UnitPrice:     data["unit_price"],
		PurchasePrice: data["purchase_price"],
		Tax:           data["tax"],
		TaxType:       data["tax_type"],
		Discount:      data["discount"],
		DiscountType:  data["Discount_type"],
		CurrentStock:  data["current_stock"],
		Status:        data["status"],
	}
	database.DB.Model(&product).Updates(product)

	return c.JSON(product)
}

func DeleteProduct(c *fiber.Ctx) error {
	id, _ := strconv.Atoi(c.Params("id"))

	product := models.Product{
		Id: uint(id),
	}

	database.DB.Delete(&product)

	return nil
}
