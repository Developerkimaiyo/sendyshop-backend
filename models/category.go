package models

import (
	"time"
)

type Category struct {
	Id        uint      `json:"id"`
	Name      string    `json:"name"`
	Icon      string    `json:"icon"`
	Product   []Product `json:"product" gorm:"foreignKey:CategoryID"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
