package models

import "time"

type Address struct {
	Id          uint   `json:"id"`
	UserID      uint   `json:"user_id"`
	ContactName string `json:"contact_name"`
	AddressType string `json:"address_type"`
	Address     string `json:"address"`
	City        string `json:"city"`
	Zip         string `json:"zip"`
	Phone       string `json:"phone"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
