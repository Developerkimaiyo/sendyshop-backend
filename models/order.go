package models

import (
	"time"
)

type Order struct {
	Id                uint   `json:"id"`
	UserId            uint   `json:"user_id"`
	DeliveryAddressId string `json:"delivery_address_id"`
	OrderAmount       string `json:"order_amount" `
	OrderStatus       string `json:"order_status"`
	PaymentStatus     string `json:"payment_status"`
	TotalTaxAmount    string `json:"total_tax_amount"`
	CreatedAt         time.Time
	UpdatedAt         time.Time
	OrderItems        []OrderItem `json:"order_items" gorm:"foreignKey:OrderId"`
}

type OrderItem struct {
	Id          uint    `json:"id"`
	ProductId   uint    `json:"product_id"`
	OrderId     uint    `json:"order_id"`
	Price       float64 `json:"price"`
	ProductName string  `json:"product_name"`
	Image       string  `json:"image"`
	Discount    string  `json:"discount"`
	Quantity    float64 `json:"quantity"`
	TaxAmount   string  `json:"tax_amount"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
