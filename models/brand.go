package models

import (
	"time"
)

type Brand struct {
	Id        uint      `json:"id"`
	Name      string    `json:"name"`
	Image     string    `json:"image"`
	Product   []Product `json:"product" gorm:"foreignKey:BrandID"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
